import random

from prnd.tests.base import PRNDTestCase
from prnd.models import VehicleType, AuctionItem


class TestVehicleTypeView(PRNDTestCase):

    def test_vehicle_type_view(self):
        self.client.force_login(self.user)

        res = self.client.get('/v1/vehicle/types/')
        self.assertEqual(res.status_code, 200)

        res = self.client.get('/v1/vehicle/types/', {'brand': '현대'})
        self.assertEqual(res.status_code, 200)

    def test_vehicle_type_count_view(self):
        for type_ in VehicleType.objects.all():
            self.create_vehicles(type_=type_, count=random.randrange(1, 5))
        self.create_auction_items()

        # LOGIN REQUIRED
        res = self.client.get('/v1/vehicle/types/count/')
        self.assertEqual(res.status_code, 401)

        self.client.force_login(self.user)
        # 브랜드
        res = self.client.get('/v1/vehicle/types/count/')
        self.assertEqual(res.status_code, 200)
        data = res.json()
        for row in data['items']:
            cnt = AuctionItem.objects.only_confirmed().filter(vehicle__type__brand=row['name']).count()
            self.assertEqual(row['count'], cnt)

        # 차종
        res = self.client.get('/v1/vehicle/types/count/', {'brand': '현대'})
        self.assertEqual(res.status_code, 200)
        data = res.json()
        for row in data['items']:
            cnt = AuctionItem.objects.only_confirmed().filter(
                vehicle__type__brand='현대',
                vehicle__type__name=row['name']
            ).count()
            self.assertEqual(row['count'], cnt)

        # REQUIRED FIELDS
        res = self.client.get('/v1/vehicle/types/count/', {'name': '그랜저'})
        self.assertEqual(res.status_code, 422)
        self.assertEqual(res.json()['code'], 'MISSING_ELEMENTS')

        # 모델
        res = self.client.get('/v1/vehicle/types/count/', {'brand': '현대', 'name': '그랜저'})
        self.assertEqual(res.status_code, 200)
        data = res.json()
        for row in data['items']:
            cnt = AuctionItem.objects.only_confirmed().filter(
                vehicle__type__brand='현대',
                vehicle__type__name='그랜저',
                vehicle__type__model=row['name'],
            ).count()
            self.assertEqual(row['count'], cnt)


class TestVehicleView(PRNDTestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.user)

    def test_vehicle_list(self):
        type_ = VehicleType.objects.first()
        self.create_vehicles(type_=type_, count=10)
        self.create_auction_items()

        res = self.client.get('/v1/vehicles/')
        self.assertEqual(res.status_code, 200)
        self.assertEqual(len(res.json()['vehicles']), 10)
