import random
import datetime
import tempfile
from PIL import Image

from django.test import TestCase, override_settings
from prnd.models import User, Dealer, Vehicle, VehicleImage, AuctionItem, Region


class PRNDTestCase(TestCase):
    user, dealer, staff, region = None, None, None, None

    def setUp(self):
        self.user = self.create_user()
        self.dealer = self.create_dealer()
        self.staff = self.create_staff()
        self.region = self.create_region()

    def create_user(self):
        return User.objects.create_user(
            email='test@test.com',
            password='testtest123',
            name='테스트유저',
            phone_number='01000000000',
        )

    def create_dealer(self):
        dealer = Dealer.objects.create(is_confirmed=True)
        return User.objects.create_user(
            email='dealer@test.com',
            password='testtest123',
            name='딜러',
            phone_number='01011111111',
            dealer=dealer
        )

    def create_staff(self):
        return User.objects.create_user(
            email='staff@test.com',
            password='testtest123',
            name='직원',
            phone_number='01022222222',
            is_staff=True
        )

    def create_region(self):
        return Region.objects.create(region='서울', sub_region='강남구')

    def get_image_file(self, temp_file):
        image = Image.new("RGBA", (10, 10), (255, 0, 0, 0))
        image.save(temp_file, 'png')
        return temp_file

    @override_settings(MEDIA_ROOT=tempfile.gettempdir())
    def create_vehicles(self, type_, count=1):
        vehicles = [
            Vehicle(
                color='검정',
                type=type_,
                grade=f'등급{_}',
                transmission=Vehicle.TransmissionChoices.AUTO.value,
                mileage=140000,
                fuel=Vehicle.FuelChoices.GASOLINE.value,
                owner=self.user,
                thumbnail='',
                manufacturing_date='2016-11-10',
                sales_year=2017
            )
            for _ in range(0, count)
        ]
        Vehicle.objects.bulk_create(vehicles)

        image_file = self.get_image_file(tempfile.NamedTemporaryFile())
        images = []
        for vehicle in vehicles:
            for _ in range(0, 5):
                images.append(VehicleImage(image=image_file.name, vehicle=vehicle))
        VehicleImage.objects.bulk_create(images)

    def create_auction_items(self, confirmed=True):
        now = datetime.datetime.now()
        auctions = []
        for idx, vehicle in enumerate(Vehicle.objects.all()):
            auction = AuctionItem(vehicle=vehicle, region=self.region)

            if confirmed:
                started_at = now - datetime.timedelta(days=random.randrange(0, 10))
                auction.started_at = started_at
                auction.expired_at = started_at + datetime.timedelta(days=2)
            auctions.append(auction)
        AuctionItem.objects.bulk_create(auctions)
