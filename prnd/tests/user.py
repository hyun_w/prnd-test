import json
import datetime

from prnd.tests.base import PRNDTestCase


class TestUser(PRNDTestCase):

    def test_signin(self):
        # SUCCESS
        res = self.client.post(
            '/v1/signin/',
            data={'email': 'test@test.com', 'password': 'testtest123'}
        )
        self.assertEqual(res.status_code, 200)
        self.client.logout()

        # JSON REQUEST TEST (MIDDLEWARE TEST)
        res = self.client.post(
            '/v1/signin/',
            data=json.dumps({'email': 'test@test.com', 'password': 'testtest123'}),
            content_type='application/json'
        )
        self.assertEqual(res.status_code, 200)
        self.client.logout()

        # PASSWORD FAIL
        res = self.client.post(
            '/v1/signin/',
            data={'email': 'test@test.com', 'password': 'testtest1234'}
        )
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.json()['code'], 'WRONG_CREDENTIALS')

        # INACTIVATED USER (SOFT DELETION)
        self.user.delete()
        res = self.client.post(
            '/v1/signin/',
            data={'email': 'test@test.com', 'password': 'testtest123'}
        )
        self.assertEqual(res.status_code, 401)
        self.assertEqual(res.json()['code'], 'INACTIVATED_USER')
