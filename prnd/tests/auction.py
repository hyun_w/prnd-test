import random
from prnd.tests import PRNDTestCase
from prnd.models import AuctionItem, VehicleType, Vehicle


class TestAuctionView(PRNDTestCase):

    def setUp(self):
        super().setUp()
        self.client.force_login(self.dealer)

    def test_auction_list(self):
        for type_ in VehicleType.objects.all():
            self.create_vehicles(type_=type_, count=random.randrange(1, 5))
        self.create_auction_items()

        res = self.client.get(f'/v1/auctions/', {'brand': '현대'})
        self.assertEqual(res.status_code, 200)

        res = self.client.get(f'/v1/auctions/', {'brand': '현대', 'name': '그랜저'})
        self.assertEqual(res.status_code, 200)

        res = self.client.get(f'/v1/auctions/', {'brand': '현대', 'name': '그랜저 XG'})
        self.assertEqual(res.status_code, 200)

    def test_auction_detail(self):
        type_ = VehicleType.objects.first()
        self.create_vehicles(type_=type_, count=1)
        self.create_auction_items(confirmed=True)

        item = AuctionItem.objects.first()

        res = self.client.get(f'/v1/auctions/{str(item.id)}/')
        self.assertEqual(res.status_code, 200)

        # permission 검사
        self.client.force_login(self.user)
        res = self.client.get(f'/v1/auctions/{str(item.id)}/')
        self.assertEqual(res.status_code, 403)


class TestAuctionCandidateView(PRNDTestCase):
    def setUp(self):
        super().setUp()
        self.client.force_login(self.staff)

    def test_auction_candidate_view(self):
        type_ = VehicleType.objects.first()
        self.create_vehicles(type_=type_, count=20)
        self.create_auction_items(confirmed=False)

        res = self.client.get('/v1/candidates/')
        self.assertEqual(res.status_code, 200)

        data = res.json()
        self.assertEqual(len(data['items']), 10)
        self.assertTrue(data['has_next_page'])
        self.assertIsNotNone(data['cursor'])

        res = self.client.get('/v1/candidates/', {'cursor': data['cursor']})
        self.assertEqual(res.status_code, 200)
        data = res.json()

        self.assertEqual(len(data['items']), 10)
        self.assertFalse(data['has_next_page'])

    def test_auction_confirm_view(self):
        type_ = VehicleType.objects.first()
        self.create_vehicles(type_=type_, count=1)
        self.create_auction_items(confirmed=False)

        item = AuctionItem.objects.only_candidates().first()
        self.assertIsNone(item.started_at)
        self.assertIsNone(item.expired_at)

        res = self.client.post(f'/v1/candidates/{str(item.id)}/confirmation/')
        self.assertEqual(res.status_code, 200)
        item = AuctionItem.objects.first()
        self.assertIsNotNone(item.started_at)
        self.assertIsNotNone(item.expired_at)



