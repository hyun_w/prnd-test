from django import forms
from django.core.exceptions import ValidationError

from prnd.models import Vehicle, VehicleType


class VehicleForm(forms.ModelForm):
    type_id = forms.UUIDField()

    class Meta:
        model = Vehicle
        exclude = ['type', 'owner', 'deleted_at', 'created_at']

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super().__init__(*args, **kwargs)

    def clean(self):
        cleaned_data = super().clean()
        type_ = None
        try:
            type_ = VehicleType.objects.get(id=cleaned_data['type_id'])
        except VehicleType.DoesNotExist:
            self.add_error('type_id', 'Could not found Type with given type_id')
        except ValidationError:
            self.add_error('type_id', 'type_id must be uuid format')

        cleaned_data['type'] = type_

        return cleaned_data

    def save(self, commit=True):
        vehicle = super().save(commit=False)
        vehicle.owner = self.user
        vehicle.type = self.cleaned_data['type']
        if commit:
            vehicle.save()
        return vehicle
