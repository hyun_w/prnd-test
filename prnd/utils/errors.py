import re


class BaseError(Exception):
    detail_format = ''

    def __init__(self, *args):
        self.args = args

    @property
    def slug(self):
        str1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', self.__class__.__name__)
        return re.sub('([a-z0-9])([A-Z])', r'\1_\2', str1).upper()

    @property
    def detail_message(self):
        return self.detail_format.format(*self.args)

    def to_dict(self):
        return {'code': self.slug, 'message': self.detail_message}


class DataNotFoundError(BaseError):
    detail_format = '{} is not found'


class DataTypeError(BaseError):
    detail_format = '{} "{}" is not valid type'


class InvalidData(BaseError):
    detail_format = '{}'
