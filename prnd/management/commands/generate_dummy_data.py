import random
import datetime

from django.core.management.base import BaseCommand
from prnd.models import User, Dealer, VehicleType, Vehicle, VehicleImage, AuctionItem, Region


class Command(BaseCommand):
    def handle(self, *args, **options):
        self.create_users()
        self.create_region()
        self.create_vehicles()
        self.create_auction_items()

    def create_users(self):
        print('Create users')
        # 유저
        user = User.objects.create_user(
            email='test@test.com',
            password='testtest123',
            name='테스트유저',
            phone_number='01000000000',
        )

        # 딜러
        user.id = None
        user.email = 'dealer@test.com'
        dealer = Dealer.objects.create(is_confirmed=True)
        user.dealer = dealer
        user.save()

        # Staff
        user.id = None
        user.dealer = None
        user.email = 'staff@test.com'
        user.is_staff = True
        user.save()
        print('Done')

    def create_region(self):
        print('Create region')
        for row in ['강남구', '강북구', '은평구', '중구']:
            Region.objects.create(region='서울시', sub_region=row)
        print('Done')

    def create_vehicles(self):
        print('Create vehicles')
        user = User.objects.filter(dealer=None, is_staff=False).first()
        vehicles = []
        grades = ['럭셔리', '프리미엄', '모던']
        for type_ in VehicleType.objects.all():
            vehicles += [
                Vehicle(
                    color='검정',
                    type=type_,
                    grade=grades[random.randrange(0, 3)],
                    transmission=Vehicle.TransmissionChoices.AUTO.value,
                    mileage=140000,
                    fuel=Vehicle.FuelChoices.GASOLINE.value,
                    owner=user,
                    thumbnail='/media/vehicle_image/pinkepie.jpg',
                    manufacturing_date='2016-11-10',
                    sales_year=2017
                )
                for _ in range(0, 5)
            ]
        Vehicle.objects.bulk_create(vehicles)
        images = [VehicleImage(image='/media/vehicle_image/pinkepie.jpg', vehicle=vehicle) for vehicle in vehicles]
        VehicleImage.objects.bulk_create(images)
        print('Done')

    def create_auction_items(self):
        print('Create auction items')
        region = Region.objects.first()
        now = datetime.datetime.now()
        auctions = []
        for idx, vehicle in enumerate(Vehicle.objects.all()):
            auction = AuctionItem(vehicle=vehicle, region=region)

            if idx > 50:
                started_at = now - datetime.timedelta(days=random.randrange(0, 10))
                auction.started_at = started_at
                auction.expired_at = started_at + datetime.timedelta(days=2)
            auctions.append(auction)
        AuctionItem.objects.bulk_create(auctions)
        print('Done')
