from django.db.models import Count

from prnd.views.base import BaseView
from prnd.forms.vehicle import VehicleForm
from prnd.models import VehicleType, VehicleImage
from prnd.utils.cursor_pagination import CursorPaginator, InvalidCursor
from prnd.utils.errors import DataTypeError


class VehicleTypeView(BaseView):
    LOGIN_REQUIRED = True
    NON_REQUIRED_FIELDS = ('brand', 'name')

    def get(self, request):
        queryset = VehicleType.objects
        brand, name, keys = request.GET.get('brand'), request.GET.get('name'), ('brand',)
        if brand:
            queryset = queryset.filter(brand=brand)
            keys = ('name',)
        if name:
            if not brand:
                return {'code': 'MISSING_ELEMENTS', 'msg': 'Missing elements', 'elements': ['brand']}, 422

            queryset = queryset.filter(name=name).order_by('-manufacturing_start_year')
            keys = ('model', 'id')

        return {'items': [
            {'id': row.get('id', None), 'name': row[keys[0]]}
            for row in queryset.values(*keys).distinct()
        ]}


class VehicleTypeCountView(BaseView):
    LOGIN_REQUIRED = True

    NON_REQUIRED_FIELDS = ('brand', 'name')

    def get(self, request):
        queryset = VehicleType.objects
        brand, name, key = request.GET.get('brand'), request.GET.get('name'), 'brand'
        if brand:
            queryset = queryset.filter(brand=brand)
            key = 'name'
        if name:
            if not brand:
                return {'code': 'MISSING_ELEMENTS', 'msg': 'Missing elements', 'elements': ['brand']}, 422
            queryset = queryset.filter(name=name).order_by('-manufacturing_start_year')
            key = 'model'

        return {'items': [
            {'name': row[key], 'count': row['cnt']}
            for row in queryset.values(key).annotate(cnt=Count('vehicles__auction_items'))
        ]}


class VehicleListView(BaseView):
    LOGIN_REQUIRED = True

    REQUIRED_FIELDS = {
        'POST': ('type_id', 'color', 'transmission', 'grade', 'mileage', 'fuel', 'manufacturing_date', 'sales_year')
    }

    def get(self, request):
        queryset = request.user.vehicles.prefetch_related('auction_items').filter(deleted_at=None)
        paginator = CursorPaginator(queryset, ('-created_at', 'id'))

        try:
            page_size = int(request.GET.get('page_size') or 10)
        except ValueError:
            return DataTypeError('page_size', request.GET.get('page_size')).to_dict(), 400
        cursor = request.GET.get('cursor') or None

        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return {'code': e.code, 'message': e.message}, 400

        return {'vehicles': [
            {
                **vehicle.to_dict(),
                **{'has_active_auction': any([auction.is_active for auction in vehicle.auction_items.all()])}
            }
            for vehicle in page
        ],
            'has_next_page': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }

    def post(self, request):
        form = VehicleForm(request.POST, user=request.user, files=request.FILES)
        if not form.is_valid():
            return {'code': 'VALIDATION_ERROR', 'message': form.errors}, 400
        vehicle = form.save(commit=False)

        images = request.FILES.getlist('images', [])
        if len(images) < 4:  # thumbnail 포함 5장.
            return {'code': 'VALIDATION_ERROR', 'message': {'images': ['You should upload more than 5 images']}}, 400

        vehicle.save()
        images = [VehicleImage(image=image, vehicle=vehicle) for image in images]
        VehicleImage.objects.bulk_create(images)

        return {'vehicle': vehicle.to_full_dict()}
