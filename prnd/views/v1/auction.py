import datetime

from django.core.exceptions import ValidationError

from prnd.views.base import BaseView
from prnd.models import AuctionItem, Region, Vehicle
from prnd.utils.errors import DataNotFoundError, DataTypeError, InvalidData
from prnd.utils.cursor_pagination import CursorPaginator, InvalidCursor


class AuctionListView(BaseView):
    DEALER_REQUIRED = True

    NON_REQUIRED_FIELDS = ('brand', 'name', 'model', 'order', 'page_size', 'cursor')

    def get(self, request):
        queryset = AuctionItem.objects.only_confirmed()
        if request.GET.get('brand'):
            queryset = queryset.filter(vehicle__type__brand=request.GET['brand'])
        if request.GET.get('name'):
            queryset = queryset.filter(vehicle__type__name=request.GET['name'])
        if request.GET.get('model'):
            queryset = queryset.filter(vehicle__type__model=request.GET['model'])

        count = queryset.count()

        order = request.GET.get('order') or 'recent'
        order = '-started_at' if order == 'recent' else 'started_at'

        try:
            page_size = int(request.GET.get('page_size') or 10)
        except ValueError:
            return {'code': 'DATA_TYPE_ERROR', 'message': 'page_size must be number'}, 400
        cursor = request.GET.get('cursor') or None

        paginator = CursorPaginator(queryset, (order, 'id'))
        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return {'code': e.code, 'message': e.message}, 400

        return {
            'count': count,
            'items': [row.to_dict() for row in page],
            'has_next_page': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }


class AuctionDetailView(BaseView):
    DEALER_REQUIRED = True

    def get(self, request, auction_id):
        try:
            item = AuctionItem.objects.get(id=auction_id)
        except AuctionItem.DoesNotExist:
            return DataNotFoundError('Auction').to_dict(), 400
        except ValidationError:
            return DataTypeError('auction_id', auction_id).to_dict(), 400
        return {'item': {**item.to_dict(), **{'vehicle': item.vehicle.to_full_dict()}}}


class AuctionCandidateView(BaseView):
    LOGIN_REQUIRED = True
    REQUIRED_FIELDS = {'POST': ('vehicle_id', 'region_id')}

    NON_REQUIRED_FIELDS = {'GET': ('page_size', 'cursor')}

    def get(self, request):
        if not request.user.is_staff:
            return {'code': 'NO_PERMISSION'}, 403
        queryset = AuctionItem.objects.only_candidates()
        paginator = CursorPaginator(queryset, ('created_at', 'id'))

        try:
            page_size = int(request.GET.get('page_size') or 10)
        except ValueError:
            return DataTypeError('page_size', request.GET.get('page_size')).to_dict(), 400
        cursor = request.GET.get('cursor') or None

        try:
            page = paginator.page(page_size=page_size, cursor=cursor)
        except InvalidCursor as e:
            return {'code': e.code, 'message': e.message}, 400

        return {
            'items': [{**item.to_dict(), **{'vehicle': item.vehicle.to_full_dict()}} for item in page],
            'has_next_page': page.has_next,
            'cursor': paginator.cursor(page[-1]) if page else None
        }

    def post(self, request):
        try:
            vehicle = request.user.vehicles.get(id=request.POST['vehicle_id'])
        except Vehicle.DoesNotExist:
            return DataNotFoundError('Vehicle').to_dict(), 400
        except ValidationError:
            return DataTypeError('vehicle_id', request.POST['vehicle_id']).to_dict(), 400

        if vehicle.auction_items.filter(started_at__isnull=False, expired_at__gte=datetime.datetime.now()).exists():
            return InvalidData('Vehicle has active auction item').to_dict(), 400

        try:
            region = Region.objects.get(id=request.POST['region_id'])
        except Region.DoesNotExist:
            return DataNotFoundError('Region').to_dict(), 400
        except ValidationError:
            return DataTypeError('region_id', request.POST['region_id']).to_dict(), 400

        auction_item = AuctionItem.objects.create(vehicle=vehicle, region=region)
        return {'item': auction_item.to_dict()}


class AuctionConfirmView(BaseView):
    STAFF_REQUIRED = True

    def post(self, request, auction_id):
        try:
            item = AuctionItem.objects.get(id=auction_id, deleted_at__isnull=True)
        except AuctionItem.DoesNotExist:
            return DataNotFoundError('Auction').to_dict(), 400
        except ValidationError:
            return DataTypeError('auction_id', auction_id).to_dict(), 400

        if item.is_confirmed:
            return {'code': 'CONFIRMATION_ERROR', 'message': 'Auction item has already been confirmed'}, 400
        item.confirm()
        return {'item': item.to_dict()}


class RegionView(BaseView):
    LOGIN_REQUIRED = True

    def get(self, request):
        queryset = Region.objects
        keys = ('region',)

        region = request.GET.get('region')
        if region:
            queryset = queryset.filter(region=region)
            keys = ('sub_region', 'id')

        return {'regions': [
            {'id': row.get('id'), 'name': row.get(keys[0])}
            for row in queryset.values(*keys).distinct()
        ]}
