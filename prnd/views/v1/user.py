from django.contrib.auth import authenticate, login, logout

from prnd.views.base import BaseView


class UserSignIn(BaseView):
    REQUIRED_FIELDS = {'POST': ('email', 'password')}

    def post(self, request):
        user = authenticate(request, email=request.POST['email'], password=request.POST['password'])
        if not user:
            return {'code': 'WRONG_CREDENTIALS', 'message': ''}, 401
        if user.deleted_at is not None:
            return {'code': 'INACTIVATED_USER', 'message': ''}, 401

        login(request, user)
        return user.to_dict()
