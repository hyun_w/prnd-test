from django.views import View
from django.contrib.auth import logout
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.utils.decorators import method_decorator

from prnd.utils.encoder import PrndEncoder


class BaseView(View):
    REQUIRED_FIELDS = dict()
    LOGIN_REQUIRED = False
    DEALER_REQUIRED = False
    STAFF_REQUIRED = False

    def get_missing_fields(self, request):
        data = getattr(request, request.method, [])
        required_fields = self.REQUIRED_FIELDS.get(request.method)
        if not required_fields:
            return []
        if not all([v in data for v in required_fields]):
            return list(set(required_fields).difference(data))
        return []

    def is_login_required(self):
        return self.LOGIN_REQUIRED or self.DEALER_REQUIRED or self.STAFF_REQUIRED

    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        missing_fields = self.get_missing_fields(request)
        if missing_fields:
            return JsonResponse(dict(code='MISSING_ELEMENTS', message='Missing elements', elements=missing_fields), status=422)

        if self.is_login_required() and not request.user.is_authenticated:
            return JsonResponse(dict(code='UNAUTHORIZED'), status=401)

        if self.is_login_required() and request.user.deleted_at:
            logout(request)
            return JsonResponse(dict(code='UNAUTHORIZED'), status=401)

        if self.DEALER_REQUIRED and not (request.user.is_staff or request.user.is_dealer):
            return JsonResponse(dict(code='NO_PERMISSION'), status=403)

        if self.STAFF_REQUIRED and request.user.is_staff is False:
            return JsonResponse(dict(code='NO_PERMISSION'), status=403)

        response = super().dispatch(request, *args, **kwargs)
        if isinstance(response, HttpResponse):
            return response
        if isinstance(response, tuple):
            return JsonResponse(response[0], encoder=PrndEncoder, status=response[1])
        if isinstance(response, dict):
            return JsonResponse(response, encoder=PrndEncoder)
        if isinstance(response, list):
            return JsonResponse(response, encoder=PrndEncoder, safe=False)
        raise TypeError
