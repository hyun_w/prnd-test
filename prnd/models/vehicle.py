import uuid
import datetime

from django.db import models

from prnd.models.enum import ChoiceEnum


class VehicleType(models.Model):
    class Meta:
        verbose_name = '차종'
        verbose_name_plural = verbose_name

        index_together = ('brand', 'name', 'manufacturing_start_year')

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    brand = models.CharField(
        verbose_name='브랜드',
        max_length=31,
        db_index=True
    )
    name = models.CharField(
        verbose_name='차종',
        max_length=31,
    )
    model = models.CharField(
        verbose_name='모델',
        max_length=31,
    )
    manufacturing_start_year = models.PositiveIntegerField(
        verbose_name='생산시작 연도',
        default=0
    )
    manufacturing_end_year = models.PositiveIntegerField(
        verbose_name='생산종료 연도',
        null=True,
        blank=True
    )

    def to_dict(self):
        return {
            'id': self.id,
            'name': self.name,
            'model': self.model
        }


class VehicleQuerySet(models.QuerySet):
    def only_active(self):
        return self.filter(deleted_at__isnull=True)

    def delete(self):
        return self.update(deleted_at=datetime.datetime.now())


class VehicleManager(models.Manager):
    def get_queryset(self):
        return VehicleQuerySet(self.model, using=self._db).select_related('type').prefetch_related('images')


class Vehicle(models.Model):
    class Meta:
        verbose_name = '차량'
        verbose_name_plural = verbose_name

    objects = VehicleManager()

    class TransmissionChoices(ChoiceEnum):
        AUTO = '자동'
        MANUAL = '수동'

    class FuelChoices(ChoiceEnum):
        LPG = 'LPG'
        GASOLINE = '휘발유'
        DIESEL = '디젤'
        HYBRID = '하이브리드'
        ELECTRIC = '전기'
        BIFUEL = '바이퓨얼'

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    type = models.ForeignKey(
        'VehicleType',
        on_delete=models.SET_NULL,
        related_name='vehicles',
        null=True
    )
    color = models.CharField(  # 필요에 따라 ENUM 혹은 TABLE 로 만들어야 할 수 있음.
        verbose_name='색상',
        max_length=15
    )
    transmission = models.CharField(
        verbose_name='변속기',
        max_length=15,
        choices=TransmissionChoices.choices(),
    )
    grade = models.CharField(
        verbose_name='등급',
        max_length=255,
        help_text='HG240 럭셔리'
    )
    mileage = models.PositiveIntegerField(
        verbose_name='주행거리',
        default=0
    )
    fuel = models.CharField(
        verbose_name='연료',
        max_length=15,
        choices=FuelChoices.choices(),
    )
    owner = models.ForeignKey(
        'User',
        on_delete=models.SET_NULL,
        related_name='vehicles',
        null=True
    )
    thumbnail = models.ImageField(
        upload_to='vehicle_image',
        verbose_name='썸네일'
    )
    manufacturing_date = models.DateField(
        verbose_name='생산 날짜'
    )
    sales_year = models.PositiveIntegerField(
        verbose_name='연형'
    )
    deleted_at = models.DateTimeField(
        verbose_name='삭제 날짜',
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    @property
    def thumbnail_url(self):
        return self.thumbnail.url if self.thumbnail else None

    @property
    def image_urls(self):
        return [image.url for image in self.images.all() if image.url]

    def to_dict(self):
        return {
            'id': self.id,
            'type': self.type.to_dict(),
            'grade': self.grade,
            'mileage': self.mileage,
            'manufacturing_date': self.manufacturing_date.strftime('%Y-%m'),
            'sales_year': self.sales_year,
            'thumbnail': self.thumbnail_url,
            'created_at': self.created_at
        }

    def to_full_dict(self):
        return {
            **self.to_dict(),
            **{'color': self.color, 'transmission': self.transmission, 'fuel': self.fuel, 'images': self.image_urls}
        }

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.datetime.now()
        self.save()


class VehicleImage(models.Model):
    class Meta:
        verbose_name = '차량 사진'
        verbose_name_plural = verbose_name

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    image = models.ImageField(
        upload_to='vehicle_image',
        verbose_name='이미지'
    )
    vehicle = models.ForeignKey(
        'Vehicle',
        related_name='images',
        on_delete=models.CASCADE
    )

    @property
    def url(self):
        return self.image.url if self.image else None
