from enum import Enum


class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return [(x.value, x.name) for x in cls]
