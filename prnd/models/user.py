import uuid
import datetime

from django.db import models
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import BaseUserManager, AbstractBaseUser


class UserQuerySet(models.QuerySet):
    def delete(self):
        self.update(deleted_at=datetime.datetime.now())


class MyUserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        if not email:
            raise ValueError('The given username must be set')
        email = self.model.normalize_username(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)

        return user


class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        verbose_name = '회원'
        verbose_name_plural = verbose_name

    USERNAME_FIELD = 'email'

    objects = MyUserManager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    email = models.EmailField(
        verbose_name='email',
        max_length=255,
        unique=True
    )
    name = models.CharField(
        verbose_name='name',
        max_length=15,
    )
    phone_number = models.CharField(
        verbose_name='전화번호',
        max_length=15,
        blank=True
    )
    dealer = models.OneToOneField(
        'Dealer',
        on_delete=models.SET_NULL,
        related_name='user',
        null=True,
        blank=True,
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    deleted_at = models.DateTimeField(
        null=True
    )
    is_staff = models.BooleanField(
        default=False,
    )

    def __str__(self):
        return self.email

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.datetime.now()
        self.save()

    @property
    def is_dealer(self):
        if not self.dealer:
            return False
        return self.dealer.is_confirmed

    def to_dict(self):
        return {
            'id': self.id,
            'email': self.email,
            'name': self.name,
            'phone_number': self.phone_number,
            'is_dealer': self.is_dealer,
        }


class Dealer(models.Model):
    """
    딜러에 관한 명세가 없기 때문에 confirm 을 받은 딜러인지 여부와 딜러시작 날짜만 저장함.
    """
    class Meta:
        verbose_name = '딜러정보'
        verbose_name_plural = verbose_name

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    is_confirmed = models.BooleanField(
        default=False
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )
