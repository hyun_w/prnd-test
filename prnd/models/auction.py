import uuid
import datetime

from django.db import models


class AuctionItemQuerySet(models.QuerySet):
    def only_candidates(self):
        return self.filter(deleted_at__isnull=True, started_at__isnull=True, expired_at__isnull=True)

    def only_confirmed(self):
        return self.filter(deleted_at__isnull=True, started_at__isnull=False, expired_at__isnull=False)

    def only_active(self):
        return self.only_confirmed().filter(expired_at__isnull__gte=datetime.datetime.now())

    def delete(self):
        return self.update(deleted_at=datetime.datetime.now())


class AuctionItemManager(models.Manager):
    def get_queryset(self):
        return AuctionItemQuerySet(self.model, using=self._db).select_related(
            'region', 'vehicle__type'
        ).prefetch_related(
            'vehicle__images'
        )


class AuctionItem(models.Model):
    class Meta:
        verbose_name = '경매상품'
        verbose_name_plural = verbose_name

    objects = AuctionItemQuerySet.as_manager()

    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    vehicle = models.ForeignKey(
        'Vehicle',
        on_delete=models.SET_NULL,
        related_name='auction_items',
        null=True
    )
    region = models.ForeignKey(
        'Region',
        on_delete=models.SET_NULL,
        related_name='auction_items',
        null=True
    )
    started_at = models.DateTimeField(
        verbose_name='경매 시작 날짜',
        db_index=True,
        null=True,
        blank=True
    )
    expired_at = models.DateTimeField(
        verbose_name='경매 종료 날짜',
        db_index=True,
        null=True,
        blank=True
    )
    deleted_at = models.DateTimeField(
        verbose_name='삭제 날짜',
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    def delete(self, using=None, keep_parents=False):
        self.deleted_at = datetime.datetime.now()
        self.save()

    def confirm(self):
        now = datetime.datetime.now()
        self.started_at = now
        self.expired_at = now + datetime.timedelta(hours=48)
        self.save()
        return True

    @property
    def is_confirmed(self):
        return bool(self.started_at and self.expired_at)

    @property
    def is_active(self):
        """
        경매 진행중인 경우에만 True
        :return: boolean
        """
        return self.expired_at > datetime.datetime.now() if self.expired_at else False

    def to_dict(self):
        return {
            'id': self.id,
            'region': self.region.to_string(),
            'vehicle': self.vehicle.to_dict(),
            'started_at': self.started_at,
            'expired_at': self.expired_at,
            'is_active': self.is_active
        }
