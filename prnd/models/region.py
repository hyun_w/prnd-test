from django.db import models


class Region(models.Model):
    """
    행정시/행정구 단위까지만 저장함. 필요에 따라 행정동 단위까지 저장해야 할 수 있음.
    """
    class Meta:
        verbose_name = '지역'
        verbose_name_plural = verbose_name

    region = models.CharField(
        verbose_name='행정시 명(한)',
        max_length=20,
        db_index=True
    )
    sub_region = models.CharField(
        verbose_name='행정구 명(한)',
        max_length=20,
        blank=True
    )

    def to_dict(self):
        return {'id': self.id, 'region': self.region, 'sub_region': self.sub_region}

    def to_string(self):
        return f'{self.region} {self.sub_region}'
