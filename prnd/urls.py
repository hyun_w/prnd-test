from django.urls import path

from prnd.views import v1


urlpatterns = [
    path('v1/signin/', v1.UserSignIn.as_view()),
    path('v1/vehicle/types/', v1.VehicleTypeView.as_view()),
    path('v1/vehicle/types/count/', v1.VehicleTypeCountView.as_view()),
    path('v1/vehicles/', v1.VehicleListView.as_view()),
    path('v1/auctions/', v1.AuctionListView.as_view()),
    path('v1/auctions/<str:auction_id>/', v1.AuctionDetailView.as_view()),
    path('v1/candidates/', v1.AuctionCandidateView.as_view()),
    path('v1/candidates/<str:auction_id>/confirmation/', v1.AuctionConfirmView.as_view()),
    path('v1/regions/', v1.RegionView.as_view()),
]
