#!/bin/bash
echo "DEBUG = True" >> local_settings.py
(
virtualenv .venv -ppython3;
source ./.venv/bin/activate;
pip install -r requirements.txt;
python manage.py migrate;
python manage.py generate_dummy_data;
python manage.py runserver
)
