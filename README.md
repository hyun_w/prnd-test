# PRND-TEST

### Description
#####1. Apis
```
1. 로그인 
2. 지역 리스트 
3. 차종 리스트 
3. 차량 등록/리스트 
5. 경매 신청/컨펌/리스트/디테일
```
#####2. Models
1. User는 일반, 딜러, 스탭 권한이 있으며 딜러는 경매 리스트/디테일, 스탭은 경매 컨펌 api를 사용 할 수 있음.
2. 하나의 차량을 여러번 경매 할 수 있다는 가정하에 차량과 경매 테이블을 분리함.
```
- User models
    * User
    * Dealer
- Vehicle models
    * VehicleType: 차종
    * Vehicle: 차량
    * VehicleImage: 차량 이미지
- Auction
    * AuctionItem: 차량 경매 내역
- Region
    * Region: 지역(시,구)
```

### Requirements
```bash
python >= 3.5
virtualenv
```

### Installation
```bash
$ git clone https://hyun_w@bitbucket.org/hyun_w/prnd-test.git
 ```
 
#####install automatically
```bash
$ cd prnd-test && chmod +x run.sh
$ ./run.sh
```
#####or manually
```bash
$ echo "DEBUG = True" >> local_settings.py
$ virtualenv .venv -ppython3
$ source ./.venv/bin/activate
$ pip install -r requirements.txt
$ python manage.py migrate
$ python manage.py generate_dummy_data
```

### For dummy data
```bash
$ python manage.py generate_dummy_data
```
> 차종 데이터 생성 스크립트: 0002_data_migration.py.  
> run.sh는 dummy data 생성 스크립트를 포함하고 있음.  
>테스트 계정
>>User: test@test.com  
>>Dealer: dealer@test.com  
>>Staff: staff@test.com  
>>
>>password: testtest123

### For test
```bash
$ python manage.py test
```
### API Documentation
<https://prnd.docs.apiary.io>